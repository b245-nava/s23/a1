// console.log("Team Rocket's blasting off again");

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		Kanto: ["Brock", "Misty"],
		Hoenn: ["May", "Max"],
	}
}
console.log(trainer);

trainer.talk = function(){
	console.log("Pikachu! I choose you!");
}

console.log("Result of dot notation:")
console.log(trainer.name);
console.log("Result of bracket notation:")
console.log(trainer["age"]);
console.log("Result of talk method:")
trainer.talk();

function Pokemon(name, level){
	// PokeProperties
	this.pokemonName = name;
	this.pokemonLevel = level;
	this.pokemonHealth = 2*level;
	this.pokemonAttack = level;

	//PokeMethods
	this.faint = function(){
		console.log(this.pokemonName + " fainted!");
	}
	this.tackle = function(targetPokemon){
		console.log(this.pokemonName+ " tackled " +targetPokemon.pokemonName+ "!");
		let hpAfterTackle = targetPokemon.pokemonHealth - this.pokemonAttack
		console.log(targetPokemon.pokemonName+ "'s health fell to " +hpAfterTackle+ "!");
		targetPokemon.pokemonHealth = hpAfterTackle
		if(targetPokemon.pokemonHealth <= 0){
			targetPokemon.faint();
		}
	}
}

console.log("Pokedex:")
let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);
let geodude = new Pokemon("Geodude", 8);
console.log(geodude);
let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);

console.log("Lite Battle Simulation:")
geodude.tackle(pikachu);
console.log(pikachu);
pikachu.tackle(mewtwo);
console.log(mewtwo);
mewtwo.tackle(geodude);
console.log(geodude);



let nurse = {
	// nurse properties
	name: "Nurse Joy",
	age: "'It's rude to ask a woman her age!'",
	pokemon: ["Chansey"],
	friends: ["Officer Jenny", "all trainers"]
}

nurse.heal = function(targetPokemon){
		targetPokemon.pokemonHealth = 2*targetPokemon.pokemonLevel
		console.log("Your Pokemon are fully rested. We hope to see you again!");
	}
console.log(nurse);
nurse.heal(pikachu);
console.log(pikachu);
nurse.heal(geodude);
console.log(geodude);
nurse.heal(mewtwo);
console.log(mewtwo);